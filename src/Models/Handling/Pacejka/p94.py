# Pacejka94
from Models.Handling.Pacejka.lateral.p94_lateral import P94Lateral
from Models.Handling.Pacejka.longitudinal.p94_longitudinal import P94Longitudinal
from ISaveLoad import ISaveLoad


class Pacejka94(P94Lateral, P94Longitudinal, ISaveLoad):
    def __init__(self, name=""):
        P94Lateral.__init__(self)
        P94Longitudinal.__init__(self)
        ISaveLoad.__init__(self, name, ".p94", "0.0.0")

    def init_values(self):
        self._initialize_longitudinal_values_()
        self._initialize_lateral_values_()

    def Fx(self, Fz, SA, SR, IA, P):
        return self._calculate_longitudinal_force_(Fz, SA, SR, IA, P)

    def Fy(self, Fz, SA, SR, IA, P):
        return self._calculate_lateral_force_(Fz, SA, SR, IA, P)


# Save object
    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = Aerodynamics_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "a0": self.a0,
            "a1": self.a1,
            "a2": self.a2,
            "a3": self.a3,
            "a4": self.a4,
            "a5": self.a5,
            "a6": self.a6,
            "a7": self.a7,
            "a8": self.a8,
            "a9": self.a9,
            "a10": self.a10,
            "a11": self.a11,
            "a12": self.a12,
            "a13": self.a13,
            "a14": self.a14,
            "a15": self.a15,
            "a16": self.a16,
            "a17": self.a17,
            "b0": self.b0,
            "b1": self.b1,
            "b2": self.b2,
            "b3": self.b3,
            "b4": self.b4,
            "b5": self.b5,
            "b6": self.b6,
            "b7": self.b7,
            "b8": self.b8,
            "b9": self.b9,
            "b10": self.b10,
            "b11": self.b11,
            "b12": self.b12,
            "b13": self.b13,
        }

        self.save_to_json(file_path, data)  #Call the json method to save the file

    # Load Object
    def load_object(self, file):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = Aerodynamics_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.aer") #Load

        """
        data = self.load_from_json(file)

        #Get the parameters from the json file.
        self.a0 = data.get("a0")
        self.a1 = data.get("a1")
        self.a2 = data.get("a2")
        self.a3 = data.get("a3")
        self.a4 = data.get("a4")
        self.a5 = data.get("a5")
        self.a6 = data.get("a6")
        self.a7 = data.get("a7")
        self.a8 = data.get("a8")
        self.a9 = data.get("a9")
        self.a10 = data.get("a10")
        self.a11 = data.get("a11")
        self.a12 = data.get("a12")
        self.a13 = data.get("a13")
        self.a14 = data.get("a14")
        self.a15 = data.get("a15")
        self.a16 = data.get("a16")
        self.a17 = data.get("a17")
        self.b0 = data.get("b0")
        self.b1 = data.get("b1")
        self.b2 = data.get("b2")
        self.b3 = data.get("b3")
        self.b4 = data.get("b4")
        self.b5 = data.get("b5")
        self.b6 = data.get("b6")
        self.b7 = data.get("b7")
        self.b8 = data.get("b8")
        self.b9 = data.get("b9")
        self.b10 = data.get("b10")
        self.b11 = data.get("b11")
        self.b12 = data.get("b12")
        self.b13 = data.get("b13")

    # endregion