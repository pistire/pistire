# Pacejka94
import numpy as np

class P94Longitudinal:
    def __init__(self):
        self._b0 = 0
        self._b1 = 0
        self._b2 = 0
        self._b3 = 0
        self._b4 = 0
        self._b5 = 0
        self._b6 = 0
        self._b7 = 0
        self._b8 = 0
        self._b9 = 0
        self._b10 = 0
        self._b11 = 0
        self._b12 = 0
        self._b13 = 0

    def _initialize_longitudinal_values_(self):
        self._b0 = 1.5
        self._b1 = 0
        self._b2 = 1100
        self._b3 = 0
        self._b4 = 300
        self._b5 = 0
        self._b6 = 0
        self._b7 = 0
        self._b8 = -2
        self._b9 = 0
        self._b10 = 0
        self._b11 = 0
        self._b12 = 0
        self._b13 = 0

    def _calculate_longitudinal_force_(self, Fz, SA, SR, IA, P):

        C = self.b0
        D = Fz * (self.b1 * Fz + self.b2)
        BCD = (self.b3 * Fz * Fz + self.b4 * Fz) * np.exp(-self.b5 * Fz)
        B = BCD / (C * D)
        H = self.b9 * Fz + self.b10
        E = (self.b6 * Fz * Fz + self.b7 * Fz + self.b8) * (1 - self.b13 * np.sign(SR + H))
        V = self.b11 * Fz + self.b12
        Bx1 = B * (SR + H)

        F = D * np.sin(C * np.arctan(Bx1 - E * (Bx1 - np.arctan(Bx1)))) + V

        return F

    @property
    def b0(self):
        return self._b0

    @b0.setter
    def b0(self, value):
        self._b0 = value

    @property
    def b1(self):
        return self._b1

    @b1.setter
    def b1(self, value):
        self._b1 = value

    @property
    def b2(self):
        return self._b2

    @b2.setter
    def b2(self, value):
        self._b2 = value

    @property
    def b3(self):
        return self._b3

    @b3.setter
    def b3(self, value):
        self._b3 = value

    @property
    def b4(self):
        return self._b4

    @b4.setter
    def b4(self, value):
        self._b4 = value

    @property
    def b5(self):
        return self._b5

    @b5.setter
    def b5(self, value):
        self._b5 = value

    @property
    def b6(self):
        return self._b6

    @b6.setter
    def b6(self, value):
        self._b6 = value

    @property
    def b7(self):
        return self._b7

    @b7.setter
    def b7(self, value):
        self._b7 = value

    @property
    def b8(self):
        return self._b8

    @b8.setter
    def b8(self, value):
        self._b8 = value

    @property
    def b9(self):
        return self._b9

    @b9.setter
    def b9(self, value):
        self._b9 = value

    @property
    def b10(self):
        return self._b10

    @b10.setter
    def b10(self, value):
        self._b10 = value

    @property
    def b11(self):
        return self._b11

    @b11.setter
    def b11(self, value):
        self._b11 = value

    @property
    def b12(self):
        return self._b12

    @b12.setter
    def b12(self, value):
        self._b12 = value

    @property
    def b13(self):
        return self._b13

    @b13.setter
    def b13(self, value):
        self._b13 = value