from Models.Handling.Pacejka.p94 import Pacejka94
from Fitting.Pacejka.p94 import p94_fit_lateral, p94_fit_longitudinal

def fit_model(model, data, fit_type, Fz, SA, IA):
    if type(model) is Pacejka94 and fit_type == "Lateral":
        return p94_fit_lateral.fit_lateral(model=model, data=data, Fz=Fz,SA= SA,IA= IA)
    elif model is Pacejka94 and fit_type == "Longitudinal":
        p94_fit_longitudinal.fit_longitudinal(model)