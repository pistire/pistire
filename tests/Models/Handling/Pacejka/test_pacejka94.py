from Models.Handling.Pacejka.p94 import Pacejka94


def test_model_coefficients():
    tire_model = Pacejka94()
    tire_model.init_values()
    assert tire_model.a0 == 1.4, "Should be 1.4"

def test_model_save_load():
    pass

def test_model_fx_values():
    pass

def test_model_fy_values():
    pass