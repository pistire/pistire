from Charts.Templates import ChartTemplates
from Models.Handling.Pacejka.p94 import Pacejka94
import numpy as np

def test_charts():
    p94 = Pacejka94()
    p94.init_values()


    #ChartTemplates.plot_Fx_SR(p94, Fz=[2, 4], SR=np.linspace(-20, 20, 50), SA=0, IA=0,P=0)
    ChartTemplates.plot_Fy_SA(p94, Fz=[2, 4], SA=np.linspace(-20, 20, 50), IA=[0], SR=[], P=[])
