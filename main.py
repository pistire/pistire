from Charts.Templates import ChartTemplates
from Models.Handling.Pacejka.p94 import Pacejka94
import numpy as np
from random import random
import matplotlib.pyplot as plt
from Fitting import fit_tire_model

p94 = Pacejka94()
p94.init_values()

# ChartTemplates.plot_Fx_SR(p94, Fz=[2, 4], SR=np.linspace(-20, 20, 50), SA=0, IA=0,P=0)
#ChartTemplates.plot_Fy_SA(p94, FZ=[2], SA=[np.linspace(-20, 20, 50)], IA=[0], SR=[0], P=[0])
y=[]
y_f=[]
for i in np.linspace(-20, 20, 50):
    y_c = p94._calculate_lateral_force_(2,i,0,0,0)
    y_f.append(y_c)
    y.append(y_c+random()*100)


_, ax = plt.subplots()

ax.scatter(np.linspace(-20, 20, 50), y)
ax.plot(np.linspace(-20, 20, 50), y_f)

#p94.fnc2min(Fz=2, SA=np.linspace(-20, 20, 50), IA=0, data=y)
p94 = fit_tire_model.fit_model(model=p94, data=y, fit_type="Lateral", Fz=2, SA=np.linspace(-20, 20, 50), IA=0)
y_ff = []
for i in np.linspace(-20, 20, 50):
    y_c = p94._calculate_lateral_force_(2,i,0,0,0)
    y_ff.append(y_c)
ax.plot(np.linspace(-20, 20, 50), y_ff)

plt.show()